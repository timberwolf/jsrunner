var editor = null

var editingScriptIndex = null

function setupEditor() {
	let container = document.getElementById("script-editor-container")
	editor = CodeMirror(container, {
		mode: "javscript",
		lineNumbers: true,
		indentWithTabs: true,
		indentUnit: 4,
		styleSelectedText: true
	})
}
setupEditor()

async function getScriptsFromStorage() {
	return browser.storage.local.get("scripts")
	.then(function(storageResults) {
		if (storageResults.scripts == undefined || storageResults.scripts == null) {
			return []
		}
		return storageResults.scripts
	}).catch(function(error) {
		console.log("*** Couldn't get scripts from local storage: " + error +
		            "\n[this is probably not an error, just none are saved yet]")
		return []
	})
}

function glomSitePatterns(patterns) {
	return patterns.join(",")
}
function unglomSitePatterns(patternsStr) {
	return patternsStr.split(",")
}

async function checkForUnsavedWork() {
	let clean = editor.isClean()
	console.log("Editor clean: " + clean)
	return clean
	
	if (editingScriptIndex == null) {
		console.log("There doesn't appear to be a current script")
		return Promise.resolve(false)
	}
	return getScriptsFromStorage()
	.then(function(scripts) {
		let script = scripts[editingScriptIndex]
		
		let nameField = document.getElementById("script-savename-field")
		let patternField = document.getElementById("script-savepattern-field")
		// let textarea = document.getElementById("script-editor")
		
		if (patternField.value != glomSitePatterns(script.sitePatterns) ||
		editor.getValue != script.scriptContent) {
			// UNSAVED
			console.log("Unsaved!")
			return true
		} else {
			return false
		}
	})
}

function populateTable() {
	getScriptsFromStorage()
	.then(function(scripts) {
		let tbody = document.getElementById("script-list-table").querySelector("tbody")
		tbody.innerHTML = "" // clear all previous table items
		
		for (let i = 0; i < scripts.length; i++) {
			let script = scripts[i]
			
			let newRow = document.createElement("tr")
			let c0 = document.createElement("td")
			let c1 = document.createElement("td")
			let c2 = document.createElement("td")
			
			// c0.textContent = i
			c0.classList = ["script-list-enabled-checkbox-column"]
			{
				let checkbox = document.createElement("input")
				checkbox.type = "checkbox"
				if (script.enabled) {
					checkbox.checked = true
				}
				
				checkbox.addEventListener("click", function(event) {
					event.stopPropagation() // don't pass the click onto the row
				})
				c0.addEventListener("click", function(event) {
					event.stopPropagation()
					checkbox.checked = !checkbox.checked
					// tell the checkbox that its state changed
					let changeEvent = new CustomEvent("change")
					checkbox.dispatchEvent(changeEvent)
				})
				c0.addEventListener("dblclick", function(event) {
					event.stopPropagation() // don't pass the click onto the row
				})
				
				checkbox.addEventListener("change", function(event) {
					console.log("Toggling script " + i)
					// save
					let enabled = checkbox.checked
					getScriptAtIndex(i).then(function(script) {
						console.log(JSON.stringify(script, null, "\t"))
						if (script == null || script == undefined) {
							console.log("Couldn't get script to toggle!")
							return
						}
						script.enabled = enabled
						replaceScriptAtIndex(i, script)
					})
				})
				
				let wrapper = document.createElement("div")
				wrapper.appendChild(checkbox)
				c0.appendChild(wrapper)
			}
			
			c1.textContent = script.name
			c2.textContent = glomSitePatterns(script.sitePatterns)
			
			let scriptButtonsSlideout = document.createElement("div")
			scriptButtonsSlideout.className = "script-buttons-slideout"
			{
				let predeleteButton = document.createElement("button")
				predeleteButton.className = "script-predelete-button"
				let deleteButtonImg = document.createElement("img")
				deleteButtonImg.src = "icons/DeleteButton.svg"
				predeleteButton.appendChild(deleteButtonImg)
				
				predeleteButton.addEventListener("click", function(event) {
					event.preventDefault()
					event.stopPropagation()
					// unhide actual delete button
					let row = this.parentNode.parentNode
					let button = row.querySelector(".script-delete-button")
					button.style.visibility = "visible"
					button.style.right = 0
					button.style.width = "auto"
					button.style.padding = "0px 8px 0px 8px"
				})
				
				scriptButtonsSlideout.appendChild(predeleteButton)
			}
			{
				let deleteButton = document.createElement("button")
				deleteButton.className = "script-delete-button"
				deleteButton.textContent = "Delete"
				
				deleteButton.addEventListener("click", function() {
					event.stopPropagation()
					deleteScriptAtIndex(i)
				})
				
				scriptButtonsSlideout.appendChild(deleteButton)
			}
			
			scriptButtonsSlideout.addEventListener("mouseleave", function(event) {
				// hide all delete buttons on mouse leave
				event.stopPropagation()
				let buttons = this.querySelectorAll(".script-delete-button")
				for (i = 0; i < buttons.length; i++) {
					let button = buttons[i]
					button.style = ""
				}
			}, false) // , false: invert responder chain -- top down!
			
			c2.appendChild(scriptButtonsSlideout)
			
			newRow.appendChild(c0)
			newRow.appendChild(c1)
			newRow.appendChild(c2)
			
			newRow.addEventListener("click", function(event) {
				// check to make sure we don't have any unsaved work
				console.log("Checking for unsaved work...")
				if (editor.isClean()) {
					console.log("Editor is clean, loading.")
					loadScriptAtIndex(i)
				} else {
					// let textarea = document.getElementById("script-editor")

					let editorBG = document.querySelector(".CodeMirror")

					editorBG.style.backgroundColor = "#FFFFC0"
					window.setTimeout(function() {
						editorBG.style = ""
					}, 1000)
				}
			})
			newRow.addEventListener("dblclick", function(event) {
				// skip the check and overwrite current contents
				console.log("Skipping save check and loading")
				loadScriptAtIndex(i)
			})
			
			tbody.appendChild(newRow)
		}
	})
	
	activateContentScripts()
}

function scriptSortCompare(a, b) {
	if (a.name < b.name) {
		return -1
	} else if (a.name > b.name) {
		return 1
	}
	// same name
	aStr = JSON.stringify(a)
	bStr = JSON.stringify(b)
	if (aStr < bStr) {
		return -1
	} else if (aStr > bStr) {
		return 1
	}
	// same contents, too!
	return 0
}

async function saveCurrentScript() {
	let nameField = document.getElementById("script-savename-field")
	let patternField = document.getElementById("script-savepattern-field")
	// let textarea = document.getElementById("script-editor")
	
	let name = nameField.value
	let pattern = patternField.value
	// let scriptContent = textarea.getValue()
	let scriptContent = editor.getValue()
	
	return getIndexOfScript({name: name})
	.then(function(index) {
		editingScriptIndex = index
		
		let newScript = {
			name: name,
			sitePatterns: unglomSitePatterns(pattern),
			scriptContent: scriptContent,
			enabled: true
		}
		
		console.log("Saving script at index " + index)
		return replaceScriptAtIndex(editingScriptIndex, newScript)
	}).then(function(index) {
		console.log("Index of current script is now " + index)
		editingScriptIndex = index
		editor.markClean()
		return Promise.resolve()
	})
}

async function getScriptAtIndex(i) {
	return getScriptsFromStorage()
	.then(function(scripts) {
		if (scripts.length == 0) {
			return Promise.reject(Error("No scripts available"))
		}
		return Promise.resolve(scripts[i])
	})
}
async function getIndexOfScript(script) {
	return getScriptsFromStorage()
	.then(function(scripts) {
		// get index of new script
		let index = null
		for (let i = 0; i < scripts.length; i++) {
			if (scripts[i].name == script.name) {
				index = i
				break
			}
			index = null
		}
		
		return Promise.resolve(index)
	})
}
async function replaceScriptAtIndex(index, newScript) {
	return getScriptsFromStorage()
	.then(function(scripts) {
		if (index == undefined || index == null) {
			scripts.push(newScript)
		} else {
			// replace the old one
			// scripts.splice(index, 1)
			scripts[index] = newScript
		}
		
		scripts.sort(scriptSortCompare)
		
		console.log(JSON.stringify(scripts, null, "\t"))
		
		browser.storage.local.set({
			scripts: scripts
		}).catch(function(error) {
			console.log("*** ERROR SAVING SCRIPT! *** " + error)
		})
		
		return getIndexOfScript(newScript)
	}).then(function(newIndex) {
		if (newIndex == null) {
			console.log("*** COULDN'T FIND NEWLY CREATED SCRIPT! ***")
		}
		
		populateTable()
		
		return Promise.resolve(newIndex)
	})
}

async function deleteScriptAtIndex(i) {
	return getScriptsFromStorage()
	.then(function(scripts) {
		// remove it
		scripts.splice(i, 1)
		
		scripts.sort(scriptSortCompare)
		
		if (editingScriptIndex != null && editingScriptIndex >= i) {
			editingScriptIndex-- // to compensate
		}
		
		console.log(JSON.stringify(scripts, null, "\t"))
		
		browser.storage.local.set({
			scripts: scripts
		}).catch(function(error) {
			console.log("*** ERROR SAVING SCRIPT! *** " + error)
		})
		
		populateTable()
		
		return Promise.resolve()
	})
}

async function loadScriptAtIndex(i) {
	getScriptsFromStorage()
	.then(function(scripts) {
		let script = scripts[i]
		
		editingScriptIndex = i
		
		let nameField = document.getElementById("script-savename-field")
		let patternField = document.getElementById("script-savepattern-field")
		// let textarea = document.getElementById("script-editor")
		
		nameField.value = script.name
		patternField.value = glomSitePatterns(script.sitePatterns)
		editor.setValue(script.scriptContent)
		
		editor.markClean()
		
		populateTable()
	})
}

document.getElementById("script-save-button").addEventListener("click", function(event) {
	saveCurrentScript()
	.then(function() {
		activateContentScripts()
	})
})

populateTable()

async function activateContentScripts() {
	browser.runtime.sendMessage({
		action: "InjectContentScripts"
	})
	.catch(function(error) {
		console.log("Failed to send message: " + error)
	})
}

async function exportScripts() {
	return browser.storage.local.get()
	.then(function(storageResults) {
		let jsonData = JSON.stringify(storageResults, null, "\t")
		let file = new File([jsonData], "jsrunner-scripts.json")
		let objectURL = URL.createObjectURL(file) // WARNING: TODO: MEMORY LEAK! To fix it, call URL.revokeObjectURL() after the download finishes.
		return browser.downloads.download({
			filename: "JSRunner Scripts.json",
			url: objectURL,
			saveAs: true
		})
	})
}
async function importScripts(input) {
		console.log("Got import data:", input)
		let data = JSON.parse(input)
		console.log("Importing scripts:", data)
		
		browser.storage.local.set(data).then(function(result) {
			console.log("Script import successful, reloading table.")
			populateTable()
		}).catch(function(error) {
			console.log("*** ERROR SAVING SCRIPT! *** " + error)
		})
}
document.getElementById("exportall-button").addEventListener("click", function() {
	event.stopPropagation()
	exportScripts()
	.catch(function(error) {
		console.log("Couldn't export scripts! " + error)
	})
})
document.getElementById("importall-button").addEventListener("click", function() {
	event.stopPropagation()
	document.getElementById("importall-input").click()
})
document.getElementById("importall-input").addEventListener("change", function(event) {
	let files = this.files
	let file = files[0]
	let reader = new FileReader()
	reader.onload = function(event) {
		let text = event.target.result
		importScripts(text)
	}
	reader.readAsText(file)
})

// activateContentScripts()
