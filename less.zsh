#!/bin/zsh -f

setopt extendedglob

for file in **/*.less; do
	echo "Converting $file"
	filename=${file##*/}
	# echo $file ${(S)file%%/[^/]*}/.compiled/${filename%%.less}.css
	lessc $file ${(S)file%%/[^/]*}/.compiled/${filename%%.less}.css
done
