var extensionPageTab = null

function showExtensionPage() {
	function createTab() {
		browser.tabs.create({
			active: true,
			url: "/page/index.html"
		}).then(function(tab) {
			extensionPageTab = tab
		}).catch(function(error) {
			console.log("*** Couldn't open extension page! *** " + error)
		})
	}
	if (extensionPageTab != null) {
		let windowID = extensionPageTab.windowId
		browser.windows.update(windowID, {
			focused: true
		}).then(function() {
			return browser.tabs.update(extensionPageTab.id, {
				active: true
			})
		}).then(function() {
			// do nothing since it succeeded
		}).catch(function(error) {
			console.log("Couldn't activate extension page tab: " + error)
			createTab()
		})
	} else {
		createTab()
	}
}

// register toolbar button action
browser.browserAction.onClicked.addListener(showExtensionPage)

// internal communication
browser.runtime.onMessage.addListener(function(payload) {
	switch (payload.action) {
		case "InjectContentScripts":
			activateContentScripts()
			break
	}
})

var registeredContentScripts = []

async function activateContentScripts() {
	return browser.storage.local.get("scripts")
	.then(function(storageResults) {
		if (storageResults.scripts == undefined || storageResults.scripts == null) {
			return []
		}
		return storageResults.scripts
	}).then(function(scripts) {
		console.log("Registering content scripts: " + JSON.stringify(scripts, null, "\t"))
		
		let scriptPromises = []
		for (let i = 0; i < scripts.length; i++) {
			let script = scripts[i]
			if (script.enabled == false) {
				continue
			}
			let result = Promise.resolve(Error("Unknown error while loading content script"))
			try {
				result = browser.contentScripts.register({
					matches: script.sitePatterns,
					js: [
						{
							code: script.scriptContent
						}
					]
				})
			} catch (exception) {
				console.log("Couldn't register content script: " + exception)
				result = Promise.reject(exception)
			}
			// let result = Promise.resolve(script)
			console.log("Result: " + JSON.stringify(result, null, "\t"))
			scriptPromises.push(result)
		}
		return Promise.all(scriptPromises)
	}).then(function(scriptActivationResults) {
		console.log("Script activation results:")
		for (let i = 0; i < scriptActivationResults.length; i++) {
			console.log(scriptActivationResults[i])
		}
		
		// unregister old content scripts
		for (let i = 0; i < registeredContentScripts.length; i++) {
			registeredContentScripts[i].unregister()
		}
		
		registeredContentScripts = scriptActivationResults
		
		return Promise.resolve(registeredContentScripts)
	}).catch(function(error) {
		console.log("*** Couldn't inject content scripts: " + error)
		return Promise.reject(error)
	})
}

// register scripts on browser start
activateContentScripts()
